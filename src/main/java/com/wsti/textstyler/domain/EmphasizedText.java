package com.wsti.textstyler.domain;

public class EmphasizedText extends TextDecorator {

    public EmphasizedText(Text text) {
        super(text);
    }

    @Override
    public String getText() {
        return "<em>" + text.getText() + "</em>";
    }

    @Override
    public void write() {
        System.out.println(this.getText());
    }
}
