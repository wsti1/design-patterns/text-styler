package com.wsti.textstyler.domain;

public class ParagraphText extends TextDecorator {

    public ParagraphText(Text text) {
        super(text);
    }

    @Override
    public String getText() {
        return "<p>" + text.getText() + "</p>";
    }

    @Override
    public void write() {
        System.out.println(this.getText());
    }
}
