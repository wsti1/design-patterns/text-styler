package com.wsti.textstyler.domain;

public interface Text {

    String getText();

    void write();
}
