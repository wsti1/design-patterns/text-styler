package com.wsti.textstyler.domain;

public abstract class TextDecorator implements Text {

    protected Text text;

    public TextDecorator(Text text) {
        this.text = text;
    }

    public String getText() {
        return text.getText();
    }

    public void write() {
        text.write();
    }
}
