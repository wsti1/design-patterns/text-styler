package com.wsti.textstyler;

import com.wsti.textstyler.domain.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class TextStylerApplication {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Welcome to TextStyler!");
        System.out.println("Available text tags: paragraph, strong, marked, emphasized");
        System.out.println("\n");

        System.out.print("Enter your text: ");
        String text = scanner.nextLine();

        System.out.print("Enter selected tags separated by space: ");
        String[] tags = scanner.nextLine().split(" ");

        Text result = processText(text, tags);
        System.out.println("\n");
        System.out.println("Result: " + result.getText());
    }

    private static Text processText(String text, String[] tags) {
        String[] uniqueTags = reduceDuplicates(tags);

        Text textHolder = new PlainText(text);
        for (String tag : uniqueTags) {
            textHolder = decorateText(textHolder, tag);
        }
        return textHolder;
    }

    private static String[] reduceDuplicates(String[] tags) {
        return new HashSet<>(Arrays.asList(tags)).toArray(new String[0]);
    }

    private static Text decorateText(Text text, String markup) {
        switch (markup) {
            case "paragraph":
                return new ParagraphText(text);
            case "strong":
                return new StrongText(text);
            case "marked":
                return new MarkedText(text);
            case "emphasized":
                return new EmphasizedText(text);
            default:
                throw new IllegalArgumentException("Tag not recognized: " + markup);
        }
    }
}
